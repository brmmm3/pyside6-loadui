# PySide6 LoadUi

A helper to load ui-files into a Python class.

## Example usage

``` python
from PySide6_loadUi import loadUi


class Application(QDialog):

    def __init__(self):
        loadUi(":ui/MainWindow.ui", self, {"QWebView": CustomWebView})
```