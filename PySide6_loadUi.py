# -*- coding: utf-8 -*-

from PySide6.QtCore import QMetaObject
from PySide6.QtUiTools import QUiLoader


class UiLoader(QUiLoader):
    """
    Unlike QUiLoader this class does not create a new instance
    of the top-level widget, instead it creates the user
    interface in an existing instance of the base class.
    """

    def __init__(self, baseClass, customWidgets=None):
        """
        customWidgets is a dictionary which maps class names to class objects
        for widgets that are promoted in the Qt Designer.
        """
        QUiLoader.__init__(self, baseClass)
        self._baseClass = baseClass
        self._customWidgets = customWidgets
        self._availableWidgets = set(self.availableWidgets())
        self._availableWidgets.add("Line")  # Fix for missing widget

    def createWidget(self, clsName, parent=None, name=''):
        if parent is None and self._baseClass:
            return self._baseClass
        if clsName in self._availableWidgets:
            widget = QUiLoader.createWidget(self, clsName, parent, name)
        else:
            try:
                widget = self._customWidgets[clsName](parent)
            except (TypeError, KeyError):
                raise Exception(f'No custom widget "{clsName}" found in customWidgets of UiLoader instance')
        if self._baseClass:
            setattr(self._baseClass, name, widget)
        return widget


def loadUi(uifile, baseClass=None, customWidgets=None, dirName=None):
    """
    A new instance of the top-level widget will be created if baseClass is None.
    Otherwise baseClass is extended by the user interface.
    customWidgets is a dictionary which maps class names to class objects.
    """
    loader = UiLoader(baseClass, customWidgets)
    if dirName:
        loader.setWorkingDirectory(dirName)
    widget = loader.load(uifile)
    QMetaObject.connectSlotsByName(widget)
    return widget
